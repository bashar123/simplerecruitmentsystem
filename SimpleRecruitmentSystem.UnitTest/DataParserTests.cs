﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleRecruitmentSystem.Models;
using SimpleRecruitmentSystem.Parsers;

namespace SimpleRecruitmentSystem.UnitTest
{
	[TestClass]
	public class DataParserTests
	{
		[TestMethod]
		public void ParsePersonsTxt_NullParameter_ReturnEmptyPersonList()
		{
			var persons = DataParser.ParsePersonsTxt(null);
			Assert.AreEqual(0, persons.Count);
		}

		[TestMethod]
		public void ParsePersonsTxt_ValidData_ReturnSameLengthOfPersons()
		{
			const int numberOfLines = 3;

			var items = new string [numberOfLines]
			{
				"1;FirstnameA;LastNameA;56;Red",
				"2;FirstnameB;LastNameB;45;Blue",
				"4;FirstnameD;LastNameD;52;Green"
			};

			var persons = DataParser.ParsePersonsTxt(items);

			Assert.AreEqual(numberOfLines, persons.Count);
		}

		[TestMethod]
		public void ParsePersonsTxt_OneCorruptedLine_ReturnSameLengthOfPersonsMinusTheCorruptedLine()
		{
			const int numberOfLines = 3;

			var items = new string[numberOfLines]
			{
				"1;FirstnameA;LastNameA;56Corrupted;Red",
				"2;FirstnameB;LastNameB;45;Blue",
				"4;FirstnameD;LastNameD;52;Green"
			};

			var persons = DataParser.ParsePersonsTxt(items);

			Assert.AreEqual(numberOfLines - 1, persons.Count);
		}



		[TestMethod]
		public void ParseCommentsTxt_NullParameter_ReturnEmptyCommentList()
		{
			var comments = DataParser.ParseCommentsTxt(null);
			Assert.AreEqual(0, comments.Count);
		}

		[TestMethod]
		public void ParseCommentsTxt_ValidData_ReturnSameLengthOfComments()
		{
			const int numberOfLines = 3;

			var items = new string[numberOfLines]
			{
				"1	Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
				"3	Morbi mauris erat, vestibulum nec magna ac",
				"2	Suspendisse ac consequat sapien. Integer quis tempus arcu."
			};

			var comments = DataParser.ParseCommentsTxt(items);

			Assert.AreEqual(numberOfLines, comments.Count);
		}

		[TestMethod]
		public void ParseCommentsTxt_OneCorruptedLine_ReturnSameLengthOfCommentsMinusTheCorruptedLine()
		{
			const int numberOfLines = 3;

			var items = new string[numberOfLines]
			{
				"	Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
				"3	Morbi mauris erat, vestibulum nec magna ac",
				"2	Suspendisse ac consequat sapien. Integer quis tempus arcu."
			};

			var comments = DataParser.ParseCommentsTxt(items);

			Assert.AreEqual(numberOfLines - 1, comments.Count);
		}



		[TestMethod]
		public void ParsePersonsXml_NullParameter_ReturnEmptyPersonList()
		{
			var persons = DataParser.ParsePersonsXml(null);
			Assert.AreEqual(0, persons.Count);
		}

		// TODO xml unit tests
		// test method: node not found -> return empty list
		// test method: valid xml -> return same length person list
		// test method: One Corrupted node -> return same length of comments minus the corrupted node

	}

}
