﻿using System;
using SimpleRecruitmentSystem.Extensions;
using SimpleRecruitmentSystem.InputOutput;
using SimpleRecruitmentSystem.Parsers;

namespace SimpleRecruitmentSystem
{
	class Program
	{
		static void Main(string[] args)
		{
			var personLines = FileReader.ReadTxt(@"DataFiles\Data1.txt", 1);
			var persons = DataParser.ParsePersonsTxt(personLines);

			var personsXml = FileReader.ReadXml(@"DataFiles\Data2.xml");
			var xmlPersons = DataParser.ParsePersonsXml(personsXml);
			
			var difference = xmlPersons.CompareWith(persons); // The difference will be 17 (17 = all the rows from both files combined) because of the firstNames in Data1.txt not being in CamelCase as they are in Data2.xml. The comparison is done on all properties of the Person object.


			var commentLines = FileReader.ReadTxt(@"DataFiles\Data3.txt", 1);
			var comments = DataParser.ParseCommentsTxt(commentLines);

			var personsWithComments = persons.AssignComments(comments);
			personsWithComments.Sort();

			Console.ReadLine();
		}
	}
}
