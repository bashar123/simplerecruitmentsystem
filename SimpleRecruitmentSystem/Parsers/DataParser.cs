﻿using System;
using System.Collections.Generic;
using System.Xml;
using SimpleRecruitmentSystem.Models;

namespace SimpleRecruitmentSystem.Parsers
{
	public class DataParser
	{
		public static List<Person> ParsePersonsTxt(string[] lines)
		{
			var persons = new List<Person>();

			if (lines == null)
			{
				return persons;
			}

			for (var i = 0; i < lines.Length; i++)
			{
				var personProps = lines[i].Split(';');

				try
				{
					var personId = personProps[0];
					var personFirstName = personProps[1];
					var personLastName = personProps[2];
					var personAge = personProps[3];
					var personColor = personProps[4];

					persons.Add(new Person(int.Parse(personId), personFirstName, personLastName, int.Parse(personAge), personColor));
				}

				catch (FormatException e)
				{
					Console.WriteLine($"Couldn't parse line {i + 1}. Possible integer corruption (parsing error).. " + e.Message);
				}

				catch (IndexOutOfRangeException e)
				{
					Console.WriteLine($"Couldn't parse line {i + 1}. A value is missing.. " + e.Message);
				}
			}

			return persons;
		}


		public static List<Comment> ParseCommentsTxt(string[] lines)
		{
			var comments = new List<Comment>();

			if (lines == null)
			{
				return comments;
			}

			for (var i = 0; i < lines.Length; i++)
			{
				var commentProps = lines[i].Split('\t');

				try
				{
					var personId = commentProps[0];
					var content = commentProps[1];

					comments.Add(new Comment(int.Parse(personId), content));
				}

				catch (FormatException e)
				{
					Console.WriteLine($"Couldn't parse line {i + 1}. Possible integer corruption (parsing error).. " + e.Message);
				}
				catch (IndexOutOfRangeException e)
				{
					Console.WriteLine($"Couldn't parse line {i + 1}. A value is missing.. " + e.Message);
				}
			}

			return comments;
		}


		public static List<Person> ParsePersonsXml(XmlDocument doc)
		{
			var persons = new List<Person>();

			if (doc == null)
			{
				return persons;
			}

			XmlNodeList personNodes;
			try
			{
				personNodes = doc.SelectNodes("/Data/Persons/Person");
			}
			catch (Exception e)
			{
				Console.WriteLine("Couldn't find node '/Data/Persons/Person'. Xml is corrupted.. " + e.Message);
				return persons;
			}

			for (var i = 0; personNodes != null && i < personNodes.Count; i++)
			{
				try
				{
					var personNode = personNodes[i];

					var personId = personNode.Attributes["PersonId"].Value;
					var personAge = personNode.Attributes["age"].Value;
					var personFirstName = personNode.SelectSingleNode("FirstName").InnerText;
					var personLastName = personNode.SelectSingleNode("LastName").InnerText;
					var personColor = personNode.SelectSingleNode("Color").InnerText;

					persons.Add(new Person(int.Parse(personId), personFirstName, personLastName, int.Parse(personAge), personColor));
				}
				catch (Exception e) // Format and NullPointer exceptions
				{
					Console.WriteLine($"Couldn't parse person node {i + 1}. Possible node corruption.. " + e.Message);
				}
			}

			return persons;

		}
	}
}
