﻿namespace SimpleRecruitmentSystem.Models
{
	public class Comment
	{

		public Comment(int personId, string content)
		{
			PersonId = personId;
			Content = content;
		}

		public int PersonId { get; set; }

		public string Content { get; set; }
	}
}
