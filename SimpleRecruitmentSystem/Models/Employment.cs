﻿using System;

namespace SimpleRecruitmentSystem.Models
{
	public class Employment
	{
		public Employment(string company, DateTime startDate, DateTime endDate)
		{
			Company = company;
			StartDate = startDate;
			EndDate = endDate;
		}

		public string Company { get; set; }

		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }
	}
}
