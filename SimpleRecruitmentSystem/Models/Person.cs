﻿using System;
using System.Collections.Generic;

namespace SimpleRecruitmentSystem.Models
{
	public class Person : IComparable<Person>
	{
		// TODO : Id Preferably a GUID.
		public int Id { get; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public int Age { get; set; }

		public string Color { get; set; }

		public List<string> Comments { get; set; }

		public List<Employment> Employments { get; set; }


		public Person(int id, string firstName, string lastName, int age, string color)
		{
			Id = id;
			FirstName = firstName;
			LastName = lastName;
			Age = age;
			Color = color;
		}

		// Used for sorting (by comment count/popularity)
		public int CompareTo(Person other)
		{
			return other.Comments.Count.CompareTo(Comments.Count);
		}

		
		public override bool Equals(object other)
		{
			if (other == null)
				return false;

			if (! (other is Person))
				return false;

			var otherPerson = (Person) other;

			return Id == otherPerson.Id 
			       && FirstName == otherPerson.FirstName 
				   && LastName == otherPerson.LastName
				   && Age == otherPerson.Age
				   && Color == otherPerson.Color;
		}


		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = Id;
				hashCode = (hashCode * 397) ^ (FirstName != null ? FirstName.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ Age;
				hashCode = (hashCode * 397) ^ (Color != null ? Color.GetHashCode() : 0);
				return hashCode;
			}
		}
	}
}
