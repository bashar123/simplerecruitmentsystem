﻿using System;
using System.Linq;
using System.Xml;

namespace SimpleRecruitmentSystem.InputOutput
{
	internal class FileReader
	{
		public static string[] ReadTxt(string filePath, int skip)
		{
			if (string.IsNullOrWhiteSpace(filePath))
			{
				throw new ArgumentNullException(nameof(filePath));
			}

			try
			{
				var lines = System.IO.File.ReadAllLines(filePath);
				return lines.Skip(skip).Where(l => !string.IsNullOrEmpty(l)).ToArray(); // Skips first line and removes any empty lines
			}
			catch (Exception e)
			{
				Console.WriteLine($"Couldn't read file in {filePath}. " + e.Message);
				throw;
			}
		}

		public static XmlDocument ReadXml(string filePath)
		{
			if (string.IsNullOrWhiteSpace(filePath))
			{
				throw new ArgumentNullException(nameof(filePath));
			}

			try
			{
				var doc = new XmlDocument();
				doc.Load(filePath);

				return doc;
			}
			catch (Exception e)
			{
				Console.WriteLine($"Couldn't read xml in {filePath}. " + e.Message);
				throw;
			}
		}

	}
}
