﻿using System.Collections.Generic;
using System.Linq;
using SimpleRecruitmentSystem.Models;

namespace SimpleRecruitmentSystem.Extensions
{
	public static class ListExtensions
	{
		public static List<Person> CompareWith(this List<Person> list1, List<Person> list2)
		{
			var firstNotSecond =  list1.Except(list2);
			var secondNotFirst = list2.Except(list1);

			var combined = firstNotSecond.Concat(secondNotFirst);
			 
			return combined.ToList();
		}

		// Doesn't belong here
		public static List<Person> AssignComments(this List<Person> persons, List<Comment> comments)
		{
			foreach (var person in persons)
			{
				person.Comments = comments.Where(c => c.PersonId == person.Id).Select(c => c.Content).ToList();
			}

			return persons;
		}

	}
}
