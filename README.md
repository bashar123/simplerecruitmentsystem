#Instruktioner

- Programmet ska parsa filerna Data1.txt och Data2.xml (både hemgjord och färdig parser går bra).
- Skapa en klass-struktur för representation av personobjekten som återfinns i filerna.
- Läs in data och ta fram alla skillnader i data mellan filerna Data1 och Data2.

- Programmet ska även parsa filen Data3.txt.
- Förläng klass-strukturen för att kunna att lägga till kommentarer på varje person. 
- Läs in data för kommentarer och rangordna vilken person som har flest kommentarer.

- Skapa en teoretisk tabellstruktur för att lagra ovan data kring personer i en relationsdatabas.
- Givet din teoretiska tabellstruktur, addera ytterligare möjligheten att alla personer kan ha en eller flera anställningar där varje anställning är kopplad till ett företag och har ett start- och ett slutdatum.

- Utöka din klass-struktur för att matcha den nya teoretiska tabellstrukturen